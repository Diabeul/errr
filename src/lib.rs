pub type Error = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, Error>;

#[macro_export]
macro_rules! raise {
    ($a:expr) => {{
        return Err(Box::new($a));
    }};
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn raise_works() {
        assert!(true);
    }
}
